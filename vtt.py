from scipy.integrate import ode
from scipy import interpolate
import numpy

b0 = 168.0
b1 = -0.68
b2 = -13.9
b3 = 66.02

vulnerability_data = {
    'very_vulnerable': [1, 2, 1, 7, -2, 80],
    'vulnerable': [0.578, 0.386, 0.3, 6, -1, 80],
    'medium_resistant': [0.072, 0.097, 0, 5, -1.5, 85],
    'resistant': [0.033, 0.014, 0, 3, -1, 85],
}
vuln_keys = ['k11', 'k12', 'A', 'B', 'C', 'phic']
interpolation_keys = ['T', 'phi']


def compute_mould_data(data, initial_value=0, vul_class='medium_resistant'):
    """
    This function returns an array with mould index (M) values corresponding to
    the data points in the argument.

    The data argument should be a dictionary with keys 't', 'phi' and 'T',
    with values that are arrays containing the corresponding data points.

    An initial value can also be specified, which is the assumed value of the 
    mould index at the first data point. This should have very little effect
    on the calculation.
    """
    M = mould_system(data, vul_class)
    times = data['t']

    solver = ode(M.dMdt).set_integrator("dop853", nsteps=10000)
    solver.set_initial_value(initial_value)
    k = 0
    res = []
    res.append(initial_value)
    while solver.successful() and solver.t < times[-1]:
        k += 1
        t = times[k]
        solver.integrate(t)
        res.append(solver.y)
    return res


class mould_system:
    """
    This class implements the improved VTT model. It can be used with scipy
    integration functions.

    Optionally, a vulnerability class can be specified, as per the papers.
    """

    def __init__(self, data, vul_class='medium_resistant'):
        self.data = data
        self.vul_class = vul_class
        for i, k in enumerate(vuln_keys):
            setattr(self, k, vulnerability_data[self.vul_class][i])

        # Interpolation is used to get phi and T at arbitrary time points
        for k in interpolation_keys:
            x = data['t']
            y = data[k]
            interp = interpolate.interp1d(x, y)
            setattr(self, k, interp)
        self.dry_time = 0
        self.t_previous = 0

    def f(self, t):
        phi = self.phi(t)
        T = self.T(t)
        return b0 * numpy.exp(b1 * numpy.log(T) + b2 * numpy.log(phi) + b3)

    def k1(self, M):
        if M < 1:
            return self.k11
        else:
            return self.k12

    def Mmax(self, t):
        p = self.phi(t)
        z = (self.phic - p) / (self.phic - 100)
        return self.A + self.B*z + self.C*z*z

    def k2(self, M, t):
        x = 1 - numpy.exp(2.3 * (M - self.Mmax(t)))
        return max(x, 0)

    def dry_decay(self):
        if self.dry_time < 6:
            res = -0.00133
            # return -0.032
        if 6 < self.dry_time < 24:
            res = 0
        if self.dry_time > 24:
            res = -0.000667
            # return -0.016
        return 0.5 * res

    def dMdt(self, t, M):
        """
        This is the main function used by the integrator. It returns dMdt
        i.e. the time derivative of the mould factor.
        """
        phi = self.phi(t)
        if phi < self.phic:
            self.dry_time += t - self.t_previous
            if M < 1E-12:
                return 0
            return self.dry_decay()
        else:
            self.dry_time = 0
            res = self.k1(M) * self.k2(M, t) / self.f(t)
            self.t_previous = t
            # print(res, phi, self.T(t), t, M, self.k1(M), self.k2(M, t))
            return res
