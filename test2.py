from test_csv import process_csv
from graphs import graph_data
from sample import sample_data
from scipy.integrate import ode
from scipy import interpolate
import numpy
from vtt import mould_system as mouldyboy

b0 = 168.0
b1 = -0.68
b2 = -13.9
b3 = 66.02

vulnerability_data = {
    'very_vulnerable': [1, 2, 1, 7, -2, 80],
    'vulnerable': [0.578, 0.386, 0.3, 6, -1, 80],
    'medium_resistant': [0.072, 0.097, 0, 5, -1.5, 85],
    'resistant': [0.033, 0.014, 0, 3, -1, 85],
}
vuln_keys = ['k11', 'k12', 'A', 'B', 'C', 'phic']
interpolation_keys = ['T', 'phi']


class mould_system:
    def __init__(self, data, vulnerability_class='medium_resistant'):
        self.data = data
        self.vulnerability_class = vulnerability_class
        for i, k in enumerate(vuln_keys):
            setattr(self, k, vulnerability_data[self.vulnerability_class][i])

        for k in interpolation_keys:
            x = data['t']
            y = data[k]
            interp = interpolate.interp1d(x, y)
            setattr(self, k, interp)

    def f(self, t):
        phi = self.phi(t)
        T = self.T(t)
        return b0 * numpy.exp(b1 * numpy.log(T) + b2 * numpy.log(phi) + b3)

    def k1(self, M):
        if M < 1:
            return self.k11
        else:
            return self.k12

    def Mmax(self, t):
        p = self.phi(t)
        z = (self.phic - p) / (self.phic - 100)
        return self.A + self.B*z + self.C*z*z

    def k2(self, M, t):
        x = 1 - numpy.exp(2.3 * (M - self.Mmax(t)))
        return max(x, 0)

    def dMdt(self, t, M):
        k1 = self.k1(M)
        k2 = self.k2(M, t)
        # f = self.f(t)
        res = self.k1(M) * self.k2(M, t) / self.f(t)
        # print(M, k1, k2, f, self.Mmax(t), res)
        return res

    def interp_test(self):
        1
        # t = data['t']
        # phi_orig = data['phi']
        # phi_interp = [self.phi(x) for x in t]

def f1():
    data = process_csv('device20.csv')
    # data = sample_data(100, 35, 55, 12, 55, 99, 2)
    # data = sample_data(1, 35, 55, 12, 55, 99, 2)
    data = sample_data(50, 35, 55, 12, 55, 99, 2)
    data = sample_data(200, 20, 20, 0, 97, 97, 0, 3)
    # print(data['T'][0:100])
    # M = mould_system(data, 'very_vulnerable')
    M = mouldyboy(data, 'vulnerable')

    z = M.Mmax(5)


    tmin = 0
    tmax = data['t'][-5]
    times = numpy.linspace(tmin, tmax, 100)
    times = data['t']


    solver = ode(M.dMdt).set_integrator("dop853", nsteps=10000)
    solver.set_initial_value(0)
    k = 0
    data_sol = {}
    data_sol['M'] = []
    data_sol['T'] = []
    data_sol['phi'] = []
    data_sol['t'] = times[:-1]
    while solver.successful() and solver.t < times[-1]:
        print(k)
        k += 1
        t = times[k]
        solver.integrate(t)
        data_sol['M'].append(solver.y)
        data_sol['T'].append(M.T(t))
        data_sol['phi'].append(M.phi(t))

    print(len(data_sol['M']), len(data_sol['t']))
    print('done')
    graph_data(data_sol)

