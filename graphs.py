import matplotlib.pyplot as plt
# import numpy
import os


def mkdir(d):
    d = os.path.expanduser(d)
    if not d:
        return
    if not os.path.exists(d):
        os.makedirs(d)


def graph_data(data, output_dir='graphs'):
    for key, vals in data.items():
        if key == 't':
            continue
        name = key
        # minimum, maximum = min(vals), max(vals)
        # r = numpy.linspace(minimum, maximum)
        p, = plt.plot(data['t'], vals, '.')
        # plt.yticks(numpy.linspace(minimum, maximum, 10))
        mkdir('graphs')
        filename = os.path.join('graphs', name+'.pdf')
        plt.savefig(filename)
        plt.clf()
