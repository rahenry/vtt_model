from test2 import f1
from csv_test import process_csv
from vtt import compute_mould_data
from sample import sample_data, two_time_data
import matplotlib.pyplot as plt


class data_set:
    def __init__(self, data, caption=''):
        self.data = data
        self.caption = caption

    def plot(self, ax, key):
        # print(key, self.data[key])
        ax.plot(self.data['t'], self.data[key])
        ax.set(xlabel='t (hours)', ylabel=key)


def f2():
    plot_keys = ['T', 'phi', 'M']
    data_sets = []

    # Example 1: using data from a csv file
    # This example shows no growth since phi < phic
    f1 = 'device25 - high humid.csv'
    data = process_csv(f1)
    data['M'] = compute_mould_data(data, vul_class='very_vulnerable')
    data_sets.append(data_set(data, "Higher humidity (device25)"))

    f1 = 'device20.csv'
    data = process_csv(f1, True)
    data['M'] = compute_mould_data(data)
    data_sets.append(data_set(data, "Low humidity -> zero growth (device20)"))

    data = sample_data(200, 20, 20, 0, 97, 97, 0, 1)
    data['M'] = compute_mould_data(data, vul_class='vulnerable')
    D = data_set(data, "Fig. 11 from paper (set 1)")
    data_sets.append(D)

    data = two_time_data(200, 10, 20, 58, 2, 20, 97)
    # data['M'] = compute_mould_data(data, vul_class='vulnerable')
    data['M'] = compute_mould_data(data, vul_class='vulnerable')
    D = data_set(data, "Fig. 11 from paper (set 4)")
    data_sets.append(D)

    nfigs = len(data_sets)
    nkeys = len(plot_keys)
    fig, big_axes = plt.subplots(nfigs)
    fig.tight_layout()
    for row, big_ax in enumerate(big_axes, start=1):
        dset = data_sets[row-1]
        big_ax.set_title(dset.caption)
        big_ax.tick_params(labelcolor=(1., 1., 1., 0.0),
                           top='off', bottom='off', left='off', right='off')
        big_ax._frameon = False

    print(nfigs, nkeys)
    for i in range(nfigs * nkeys):
        i = i
        col = i % nkeys + 1
        s = i - col + 1
        p = s // nkeys
        row = p + 1
        print(i, col, s, p, row)
        ax = fig.add_subplot(nfigs, len(plot_keys), i+1)
        dset = data_sets[row-1]
        dset.plot(ax, plot_keys[col-1])

    # plt.subplots_adjust(hspace=0.3)
    plt.show()
    plt.savefig('examples.pdf')


# q1()
f2()
exit()
data = sample_data(200, 20, 20, 0, 97, 97, 0, 3)
f3(data)
exit()
f2()
f1()
