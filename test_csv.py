import csv
from datetime import datetime


def parse_timestamp(x):
    return datetime.strptime(x, '%Y-%m-%d %X')


def process_csv(filename):
    R = csv.DictReader(open(filename))
    start_time = None
    res = {}
    res['t'] = []
    res['phi'] = []
    res['T'] = []
    lines = [x for x in R]
    lines.reverse()
    start_time = parse_timestamp(lines[0]['ts'])
    for i, x in enumerate(lines):
        t = parse_timestamp(x['ts']) - start_time
        res['t'].append(float(t.total_seconds())/3600)
        p = float(x['humid'])
        p *= 5.5
        p = min(100, p)
        res['phi'].append(p)
        T = float(x['temp'])
        T *= 2
        res['T'].append(T)
    return res
