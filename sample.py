import math
import numpy
# Generate sample data


def sine_func(peak, minimum, maximum, x):
    A = 0.5 * (minimum + maximum)
    B = 0.5 * (maximum - minimum)
    return A + B * numpy.sin(math.pi/12. * (x-peak))


def sample_data(days, Tmin, Tmax, Tpeak, pmin, pmax, ppeak, points_per_hour=20):
    npoints = days * 24 * points_per_hour
    res = {}
    res['t'] = []
    res['phi'] = []
    res['T'] = []
    for i in range(npoints):
        t = float(i) / points_per_hour
        phi = sine_func(ppeak, pmin, pmax, t)
        T = sine_func(Tpeak, Tmin, Tmax, t)
        res['t'].append(t)
        res['phi'].append(phi)
        res['T'].append(T)

    return res


def flat_data(days, offset, T, phi, points_per_hour):
    res = {}
    res['t'] = []
    res['phi'] = []
    res['T'] = []
    npoints = days * 24 * points_per_hour
    for i in range(npoints):
        t = float(i) / points_per_hour + offset*24
        res['t'].append(t)
        res['phi'].append(phi)
        res['T'].append(T)
    return res


def two_time_data(days, t1, T1, phi1, t2, T2, phi2, points_per_hour=1):
    t_total = 0
    switch = 0
    res = {}
    res['t'] = []
    res['phi'] = []
    res['T'] = []
    while(True):
        if t_total > days:
            break
        if switch == 0:
            new_data = flat_data(t1, t_total, T1, phi1, points_per_hour)
            t_total += t1
            if t_total > days:
                break
            for key, val in new_data.items():
                res[key] += val
        if switch == 1:
            new_data = flat_data(t2, t_total, T2, phi2, points_per_hour)
            t_total += t2
            if t_total > days:
                break
            for key, val in new_data.items():
                res[key] += val
        switch = (switch+1) % 2
    return res
